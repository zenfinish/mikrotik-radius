const express = require('express');
const { exec } = require('child_process');

const app = express();

const port = process.env.PORT || 3001;

app.listen(port, function() {
	console.log(`Running on port ${port}`);
	exec(`node --version`, (error, stdout, stderr) => {
		if (error) {
		  throw error;
		}
		console.log(stdout);
	});
});

