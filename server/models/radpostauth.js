const mysql = require('../configs/mysql.js');

class Radpostauth {

	static find() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT * FROM radpostauth
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

}

module.exports = Radpostauth;