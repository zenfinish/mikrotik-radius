const mysql = require('mysql');

const connection = mysql.createConnection({
   host: process.env.HOST_MYSQL,
   user: process.env.USER_MYSQL,
   password: process.env.PASS_MYSQL,
   database: process.env.DB_MYSQL,
});
connection.connect();

module.exports = connection;
