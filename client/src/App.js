import React, { Component } from 'react';
import { HashRouter as Router, Route, Switch, withRouter } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import Login from './views/Login.jsx';
import Dashboard from './views/Dashboard.jsx';

class App extends Component {
		
	render() {
		return (
			<Router basename={process.env.REACT_APP_BASENAME}>
				<Switch>
					<Route exact path={`${process.env.PUBLIC_URL}/`} component={Login} />
					<Route path={`${process.env.PUBLIC_URL}/dashboard`} component={Dashboard} />
				</Switch>
			</Router>
		)
	}

}

export default withRouter(App);